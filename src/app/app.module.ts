import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { FormsModule } from '@angular/forms';
import { HeroService } from './hero-service';
import { HeroComponent } from './hero/hero.component';
import { MonsterComponent } from './monster/monster.component';
import { GameComponent } from './game/game.component';
import { CommonModule } from '@angular/common';
import { MessagerComponent } from './messager/messager.component';


@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroComponent,
    MonsterComponent,
    GameComponent,
    MessagerComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
  ],
  providers: [HeroService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
