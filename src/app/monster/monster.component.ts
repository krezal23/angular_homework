import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../person';
import { HeroService } from '../hero-service';

@Component({
  selector: 'app-monster',
  templateUrl: './monster.component.html',
  styleUrls: ['./monster.component.css']
})
export class MonsterComponent implements OnInit {

  @Input()
  monster:Person[];
 
  constructor(public heroService:HeroService) { }

  ngOnInit(): void 
  {
    this.monster = this.heroService.monster;
  }

}
