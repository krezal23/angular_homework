import { Component, OnInit, Input } from '@angular/core';
import { HeroService } from '../hero-service';
import { CharackterProps } from '../CharackterProps';
import { ClericMan } from '../cleric-man';
import {MessageService} from'../message-service';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  constructor(public heroService: HeroService, public messageService: MessageService) { }

  @Input()
messages:string[];

  ngOnInit(): void {
  }
  GameInit() {
    var heros = this.heroService.getHeroList();
    var monster = this.heroService.getMonster();
    this.messageService.AddMessage('Game Started')
    this.Fight(heros, monster);
    
  }

  public Fight(heroes: Array<CharackterProps>, monster: CharackterProps[]) {
    while (true) 
    {
      for (let hero of heroes) 
      {
        let k = 10;
        let specialAttackDiceRoll = Math.floor(Math.random() * k + 1);
        if (specialAttackDiceRoll > hero.specialAttackChance) 
        {
          hero.Attack(monster[0]);
        }
        else 
        {
          if (hero instanceof ClericMan) hero.SpecialAttack(heroes);
          else hero.SpecialAttack([monster[0]]);
        }
      }

      if (monster[0].hpStat > 0 && !monster[0].isAsleep) 
      {
        let k = 10;
        let specialAttackDiceRoll = Math.floor(Math.random() * k + 1);

        if (specialAttackDiceRoll > monster[0].specialAttackChance) {
          let targetId = Math.floor(Math.random() * heroes.length);
          let target = heroes[targetId];
          monster[0].Attack(target);

          if (target.hpStat === 0) 
          {
            this.messageService.AddMessage(`${target.name} zginął marnie`); 
                        heroes = heroes.filter(function (hero) { return hero != target })
          }
        }
        else {
          monster[0].SpecialAttack(heroes)

          for (let target of heroes) 
          {
            if (target.hpStat === 0) 
            {
              this.messageService.AddMessage(`${target.name} zginął marnie`);
              heroes = heroes.filter(function (hero) { return hero != target })
            }
          }
        }
      }
      if (heroes.length === 0) {
        this.messageService.AddMessage("Gra skończona, drużyna umarła. Cała.");
        this.messageService.AddMessage(`${monster[0].name} - postaci pozostało ${monster[0].hpStat} hp`);
        return
      }
      if (monster[0].hpStat <= 0) {
        this.messageService.AddMessage("Gra skończona, SMOG został pokonany");
        for (let hero of heroes) {
          this.messageService.AddMessage(`${hero.name} - postaci pozostało ${hero.hpStat} hp`);
          return
        }
        monster[0].isAsleep = false;
      }
    }
  }
}

