import {Person} from './person';
import { CharackterProps } from './CharackterProps';
import { MessageService } from './message-service';

export class BardMan extends Person
{
    SpecialAttack(target: CharackterProps[]): void {
        target[0].isAsleep = true;
    }
    constructor(public name: string, public maxAttackStat: number, public defStat: number, public maxHp: number,public messageService:MessageService)
    {
        super(name, maxAttackStat, defStat, maxHp,messageService);
        this.specialAttackChance = 3;
    }
}
