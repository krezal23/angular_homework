import { MagicMan } from './magic-man';
import { SwordMan } from './sword-man';
import { ClericMan } from './cleric-man';
import { BardMan } from './bard-man';
import { DragonSon } from './dragon-son';
import { Person } from './person';
import { MessageService } from './message-service';

export class HeroService 
{
    messageService: MessageService;
    hero1 = new MagicMan("inż. Jerzy Zięba", 8, 2, 18,this.messageService);
    hero2 = new SwordMan("Zbigniew Stonoga", 8, 2, 18,this.messageService );
    hero3 = new BardMan("Michał Wiśniewski", 8, 2, 18,this.messageService );
    hero4 = new ClericMan("o. Mateusz", 8, 2, 18,this.messageService );
    heroes = [this.hero1,this.hero2,this.hero3,this.hero4];
    monster =[ new DragonSon("SMOOOG KRAKOWSKI", 7, 4, 233,this.messageService)];
    
    
    getHeroList():Person[]
    {
        return this.heroes;
    }
    getMonster():Person[]
    {
        return this.monster;
    }
}
