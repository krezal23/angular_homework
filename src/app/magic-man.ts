import { CharackterProps } from './CharackterProps'
import { Person } from './person';
import { MessageService } from './message-service';

export class MagicMan extends Person
{
    SpecialAttack(target: CharackterProps[]): void 
    {
        let mageDefDebuffPower = 1;
        target[0].def = mageDefDebuffPower;
        if(target[0].def <= 0)
            {
                target[0].def = 0;
            }
        
    }
    constructor(public name: string, public maxAttackStat: number, public defStat: number, public maxHp: number,public messageService:MessageService)
    {
      super(name, maxAttackStat, defStat, maxHp,messageService)
      this.specialAttackChance = 2;
    }
};