import { CharackterProps } from './CharackterProps'
import { Person } from './person';
import { MessageService } from './message-service';

export class SwordMan extends Person {
    SpecialAttack(target: CharackterProps[]): void {
        this.Attack(target[0]);
        this.Attack(target[0]);
        console.log(`Wojownik ${this.name} zastosował tajne sztuki Muminków-Ninja i zaatakował ${target[0].name} dwukrotnie!`);
    }
    constructor(public name: string, public maxAttackStat: number, public defStat: number, public maxHp: number,public messageService:MessageService) {
      super(name, maxAttackStat, defStat, maxHp,messageService)
      this.specialAttackChance = 5;
    }
 };
