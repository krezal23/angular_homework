export interface CharackterProps 
{
    hpStat: number;
    name:string;
    maxHP:number;
    def:number;
    atackStat:number;
    Attack(oponent: CharackterProps): void;
    SpecialAttack (target: CharackterProps[]): void;
    specialAttackChance: number;
    isAsleep: boolean;
}
