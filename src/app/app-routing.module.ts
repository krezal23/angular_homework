import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component';
import { GameComponent } from './game/game.component';
import { MessagerComponent } from './messager/messager.component';
import { MonsterComponent } from './monster/monster.component';


const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: 'heroes', component: HeroesComponent},
  { path:'game',component:GameComponent},
  { path :'messager',component:MessagerComponent},
  { path :'monster',component:MonsterComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
