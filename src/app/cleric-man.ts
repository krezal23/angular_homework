import {CharackterProps} from './CharackterProps';
import { Person } from './person';
import { MessageService } from './message-service';

export class ClericMan extends Person 
{
    SpecialAttack(target: CharackterProps[]): void
    {
        for (let hero of target)
        {
            let healigPower = 3;
            hero.hpStat += healigPower;
            if(hero.hpStat > hero.maxHP)
            {
                hero.hpStat = hero.maxHP;
            }
            
        }
    }
    constructor(public name: string, public maxAttackStat: number, public defStat: number, public maxHp: number,public messageService:MessageService)
    {
        super(name, maxAttackStat, defStat, maxHp,messageService)
        this.specialAttackChance = 4;
    }
}
