import {CharackterProps} from './CharackterProps'
import { Person } from './person';
import { MessageService } from './message-service';

export class DragonSon extends Person 
{
    SpecialAttack(target: CharackterProps[]): void
    {
        for (let hero of target)
        {
            this.Attack(hero);
        }
    }
    constructor(public name: string, public maxAttackStat: number, public defStat: number, public maxHp: number,public messageService:MessageService) 
    {
      super(name, maxAttackStat, defStat, maxHp, messageService)
      this.specialAttackChance = 4;
    }   
 }