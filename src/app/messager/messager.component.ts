import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from '../message-service';

@Component({
  selector: 'app-messager',
  templateUrl: './messager.component.html',
  styleUrls: ['./messager.component.css']
})
export class MessagerComponent implements OnInit 
{
  @Input()
  messages:string[];

  constructor(public messagesService: MessageService) { }

  ngOnInit(): void 
  {
    this.messages = this.messagesService.messages
  }

}
