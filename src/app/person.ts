import { CharackterProps } from './CharackterProps';
import { MessageService } from './message-service';

export abstract class Person implements CharackterProps {
    public isAsleep;
    public hpStat;
    public specialAttackChance;
    constructor(public name: string, public atackStat: number, public def: number, public maxHP: number,public messageService:MessageService)
    {
        this.hpStat = maxHP;
    }
    abstract SpecialAttack(target: Person[]): void;

    Attack(oponent: CharackterProps): void 
    {
        let damage = Math.floor( Math.random() * this.atackStat) + 1 - oponent.def;
        if (damage < 0)
        {
          damage = 0;
        }
        oponent.hpStat = oponent.hpStat - damage;
        if (oponent.hpStat < 0)
        {
           oponent.hpStat = 0;
        }
        this.messageService.AddMessage(`${this.name} zaatakował ${oponent.name} za ${damage} pozozstało ${oponent.hpStat}hp `);
    }
}
