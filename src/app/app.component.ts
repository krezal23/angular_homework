import { Component } from '@angular/core';
import {GameManager} from './game-manager';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent 
{
  title = 'RPG';
}
