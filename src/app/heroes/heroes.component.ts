import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../person';
import{HeroService}from '../hero-service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit 
{

  @Input()
  heroes: Person[];
  

   constructor(public heroService:HeroService) { }

  ngOnInit(): void 
  {
    this.heroes = this.heroService.heroes;
  }
}
